# ECCB_script
The folder accompanies the article "A nonlinear mixed-effects approach for the mechanistic interpretation of time-series transcriptomics data" by T.A Etienne, C. Roux, E. Cinquemani, L. Girbal, M. Cocaign-Bousquet, and D. Ropers. 

#### Contents ####

1- Data
The DATA folder contains the microarray datasets.

'df_all_TX_063.txt': microarray corrected for background and spline smoothed, from Esquerré et al (2014, https://doi.org/10.1093/nar/gkt1150). Used in section 3.1 of the paper.
'df_all_TX_probe_063.txt': microarray probe intensities corrected for background, corresponding to the Esquerré dataset. Used in Section 3.2 of the paper.

2- Models
Models used in this study are stored in the MLXTRAN format of Monolix (user-friendly software, free for academics; https://lixoft.com/products/monolix/).
'spline_microArray.mlxtran': NLME model for spline smoothed microarray data
'Probes_estimation.mlxtran': NLME mdoel for microarray probe intensities
'RNAseq.mlxtran': NLME model for RNAseq data

3- Estimation results
Monolix output files are provided in three folders: 'SPLINE_MICROARRAY', 'PROBES_ESTIMATION', and 'RNAseq', including the results of estimation from the three corresponding datasets. Folder organisation is described below. See the Monolix documentation for more details on these output files (https://monolix.lixoft.com/tasks/result-files-generated-monolix/).

**FOLDER CONTENTS:**

    'populationParameters.txt': estimated population parameters with SAEM.

    'predictions.txt': predictions at the observation times.

    'summary.txt': summary file with estimated population parameters and computation time

    -'FISHERINFORMATION'

        'correlationEstimatesSA.txt': correlation matrix for the parameters

        'covarianceEstimatesSA.txt': variance-covariance matrix of the estimates

    -'INDIVIDUALPARAMETERS'

        'estimatedIndividualParameters.txt': individual parameters (from SAEM, mode, and mean of the conditional distribution)

        'estimatedRandomEffects.txt': individual random effects, calculated using the population parameters, the covariates and the conditional mean

        'simulatedIndividualParameters.txt': simulated individual parameters by the conditional distribution

        'simulatedRandomEffects.txt': simulated individual random effects by the conditional distribution

    -'LOGLIKELIHOOD'

        'individualLL.txt': -2LL for each individual (one by individual even if there are occasions)

        'logLikelihood.txt': summary of the log-likelihood calculationOFV (Objective Function Value), AIC (Akaike Information Criteria), and BIC (Bayesian Information Criteria)

    -'TESTS'

        Folder 'TESTS' includes the results of the statistical tests for model assessment. The name of the files speaks for themselves:

        'correlationRandomEffects.txt'

        'correlationRandomEffectsCovariates.txt'

        'normalityIndividualParameters.txt'

        'normalityRandomEffects.txt'

        'normalityResiduals.txt'

        'symmetryResiduals.txt'

